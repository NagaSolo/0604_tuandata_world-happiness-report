*to be moved to Project's Repo Wiki*
### Overview
- 0604_tuandata_world-happiness-report
- Dashboard, Static Pages, ML projects based on dataset from World Happiness Report 2015 - 2019
- `dashboard` branch - dataset visualization
- `master` branch - static pages for notebook
- `ml` branch - machine learning